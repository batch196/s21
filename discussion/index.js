// console.log('hello');

//arrays are used to store multiple related value in a single variable
//it is created/declared using [] brackets also know as Literals

let hobbies = ["Play video games", "Read a book", "Listen to muscic"];

console.log(typeof hobbies);

let grades = [75.4,98,90,91.5];
const planets = ['Mercury','Venus','Mars', 'earth'];
let array = ['saitama', 'one punch man', 2500, true]; //can be used but not recommended in other languages of frameworks

console.log(array);
//however , since there are no problems creating arrays linke this, you may encounter exeptions to this rule in the future and in fact in other JS libraries and frameworks

/*
	mini activity
*/

let tasks = ['wake up', 'cook', 'play music','exercise', 'join bootcamp'];
console.log(tasks);

let capitalCity = ['Tokyo', 'Manila', 'Kabul','Beijing'];
console.log(capitalCity);

//each item in an array is called an element.
//array is a collection of data, as a convention, its name is usually plural.
//we can also add the values of variables as elements in an array

let userName1 = 'fighter_smith1';
let userName2 = 'georgeKyle5000';
let userName3 = 'white_knight';

let guild = [userName1,userName2,userName3];
console.log(guild);

//.length property
// the .length property of an array is number type

console.log(tasks.length);
console.log(capitalCity.length);
//in fact, even the strings have a length property which tells us the number of characters in a string

let fullName = 'Randy Orton';
console.log(fullName.length);

//we can manipulate the .length property of an array. being that .length peroperty is a number that tells the total number of elements in an array. we can also delete the last item in an array by manipulating the .length property.
//reassign the value
tasks.length = tasks.length-1;
console.log(tasks.length);
console.log(tasks);

//we could also decrement the length property of an array. that will delete the last item.
capitalCity.length--;
console.log(capitalCity);

//we cant do the same trick with a string

fullName.length = fullName.length-1;
console.log(fullName);

//if we can shorten the array by updating the length property by updating.

let theBeatles = ['John','paul', 'Ringo', 'George'];
theBeatles.length++;
console.log(theBeatles);

//accessing the elements of an array
//accessing the array elements is the most common thing we do
//this can be done through the use of array indeces

console.log(capitalCity);
//if we want to access a particular item in the array, we can do so with array indeces. each item are ordered according to their index. we can locate items in the arrat via their indeces.

//syntax: arrayName[index];
//index are number type

console.log(capitalCity[0]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log(lakersLegends[1]); //result: shaq

// what if we want to access magic fron the array?

console.log(lakersLegends[3]); // result: magic

//we can also save or store a particular array element in a variable

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//we can also update or reassign the array using index

lakersLegends[2] = 'Pau Gasol';
console.log(lakersLegends);

// mini activity

let favoriteFood = [
	'Tonkatsu',
	'Adobo',
	'Hamburger',
	'Sinigang',
	'Pizza'
];
console.log(favoriteFood);

favoriteFood[3] = 'Siomai';
favoriteFood[4] = 'Batchoy';
console.log(favoriteFood);

//accessing the last item in an array

// we could consistenly access the last item in the array by .length property value minus 1 as the index because the index starts at zero


console.log(favoriteFood[favoriteFood.length-1]);

let bullsLegends = ['jordan','pippen','rodman','rose','scalabrine','kukoc','lavine'];
console.log(bullsLegends[bullsLegends.length-1]);

//using our indices we can also add item in the array

let newArr = [];
console.log(newArr);
console.log(newArr[0]);

newArr[0] = 'Cloud Strife';
console.log(newArr);
newArr[1] = 'Aerith Gainsborough';
console.log(newArr);

//how to add if we do not know the number of items in array or add items at the end of array


newArr[newArr.length] = 'Tifa Lockhart';
console.log(newArr);

//looping over an array and iterate all items in the array.
//set the counter as the index and set a condition that as long as the current index iterated is less than the length of the array we will rub the loop

//loop over and display all items in the newArr

//check if every number in array is divisible by 5
for(let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5,12,30,46,40];
for(let index = 0; index < numArr.length; index++){
	//first loop = index = 0 = numArr[index] = numArr[0]
	//2nd loop = index = 1 = numArr[index] = numArr[1]
	//...
	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + ' is divisible by 5');
	}else {
		console.log(numArr[index] + ' is not divisible by 5')
	}
}

//multidimentional array
let chessBoard = [

	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']

]
	console.log(chessBoard);
//accessing items in a multidementional array is a bit diffferent than 1 dementional array

//to access item in an array within an array
//first locate where the location of array is
//second add its location/index within its array
	console.log(chessBoard[1][5]);

//miniactivity
//1. log a8 in the console from our chessBoard multidimentional array
//2. log h6 in the console from our chessBoard

	console.log(chessBoard[7][0]);
	console.log(chessBoard[5][7]);
	//arrName[y][x]

















